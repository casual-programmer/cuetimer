//
//  CueTimerVC.swift
//  CueTimer
//
//  Created by Aaron Trickey on 10/8/19.
//  Copyright © 2019 Aaron Trickey. All rights reserved.
//

import Cocoa

struct TimerScriptEntry: Codable {
    var seconds: Double
    var action: String
}

struct TimerScript: Codable {
    var events: [TimerScriptEntry]
}

enum TimerScriptState {
    case ready
    case running(Timer, since: Date)
    case stopped
}


class CueTimerVC: NSViewController {
    var script: [TimerScriptEntry] = [] {
        didSet {
            updateDisplay()
        }
    }

    func loadScript(at url: URL) -> Bool {
        do {
            let data = try Data(contentsOf: url)
            let script = try JSONDecoder().decode(TimerScript.self, from: data)
            self.script = script.events.sorted { a, b in a.seconds < b.seconds }

            NSDocumentController.shared.noteNewRecentDocumentURL(url)

            view.window?.title = url.lastPathComponent
            return true

        } catch {
            presentError(error)
            return false
        }
    }

    @objc func openDocument(_ sender: Any) {
        guard let window = view.window else {
            return
        }

        let panel = NSOpenPanel()
        panel.canChooseDirectories = false
        panel.canChooseFiles = true
        panel.allowedFileTypes = ["json", "cuetimer"]
        panel.beginSheetModal(for: window) { response in
            if response == .OK, let url = panel.url {
                _ = self.loadScript(at: url)
            }
        }
    }

    override func loadView() {
        view = NSView(frame: NSRect(x: 0, y: 0, width: 480, height: 320))

        [nextActionLabel, timerLabel, priorActionLabel].forEach { l in
            view.addSubview(l)
            l.translatesAutoresizingMaskIntoConstraints = false
            l.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            l.isBezeled = false
            l.drawsBackground = false
            l.isEditable = false
            l.alignment = .center
        }

        nextActionLabel.font = .monospacedDigitSystemFont(ofSize: 32, weight: .light)
        timerLabel.font = .monospacedDigitSystemFont(ofSize: 64, weight: .regular)
        priorActionLabel.font = nextActionLabel.font

        priorActionLabel.textColor = .secondaryLabelColor

        NSLayoutConstraint.activate([
            nextActionLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 16),
            timerLabel.topAnchor.constraint(equalTo: nextActionLabel.bottomAnchor, constant: 16),
            priorActionLabel.topAnchor.constraint(equalTo: timerLabel.bottomAnchor, constant: 16),
        ])

        updateDisplay()
    }

    private func updateDisplay() {
        switch state {
        case .ready:
            if let first = script.first {
                showNextAction(first, currentlyAt: 0)
            } else {
                showNoNextAction()
            }
            timerLabel.stringValue = formatSeconds(0)
            showNoPriorAction()

        case .running(_, since: let startTime):
            let secs = Date().timeIntervalSince(startTime)

            var priorIndex = -1
            var nextIndex = -1
            for (i, entry) in script.enumerated() {
                if entry.seconds < secs {
                    priorIndex = i
                } else {
                    nextIndex = i
                    break
                }
            }

            if nextIndex > -1 {
                showNextAction(script[nextIndex], currentlyAt: secs)
            } else {
                showNoNextAction()
            }

            if priorIndex > -1 {
                showPriorAction(script[priorIndex], currentlyAt: secs)
            } else {
                priorActionLabel.stringValue = ""
            }

            timerLabel.stringValue = formatSeconds(secs)

        case .stopped:
            showMessageInsteadOfNextAction("Timer Stopped")
            nextActionLabel.textColor = .secondaryLabelColor
        }
    }

    override func mouseDown(with event: NSEvent) {
        switch state {
        case .ready:
            let timer = Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { [weak self] _ in
                self?.updateDisplay()
            }
            state = .running(timer, since: Date())

        case .running(let timer, since: _):
            timer.invalidate()
            state = .stopped

        case .stopped:
            state = .ready
        }

        updateDisplay()
    }

    private func formatSeconds(_ timeInterval: TimeInterval) -> String {
        let tenths = Int(timeInterval * 10) % 10
        let secs = Int(timeInterval)
        let s = secs % 60
        let m = (secs / 60) % 60
        let h = secs / 3600

        return String(format: "%d:%02d:%02d.%01d", h, m, s, tenths)
    }

    private func showNextAction(_ entry: TimerScriptEntry, currentlyAt secs: Double) {
        let remaining = entry.seconds - secs
        let whole = Int(remaining)
        let tenths = Int(remaining * 10) % 10

        nextActionLabel.stringValue = "In \(whole).\(tenths) seconds:\r\(entry.action)"

        switch whole {
        case 5...:
            nextActionLabel.textColor = .secondaryLabelColor
        default:
            nextActionLabel.textColor = .controlAccentColor
        }
    }

    private func showNoNextAction() {
        nextActionLabel.textColor = .secondaryLabelColor
        nextActionLabel.stringValue = "\r(no upcoming actions)"
    }

    private func showMessageInsteadOfNextAction(_ message: String) {
        nextActionLabel.stringValue = "\r\(message)"
    }

    private func showPriorAction(_ entry: TimerScriptEntry, currentlyAt secs: Double) {
        priorActionLabel.stringValue = "Previously:\r\(entry.action)"
    }

    private func showNoPriorAction() {
        priorActionLabel.stringValue = ""
    }

    private let nextActionLabel = NSTextField()
    private let timerLabel = NSTextField()
    private let priorActionLabel = NSTextField()
    private var state: TimerScriptState = .ready
}
