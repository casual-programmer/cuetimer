//
//  AppDelegate.swift
//  CueTimer
//
//  Created by Aaron Trickey on 10/8/19.
//  Copyright © 2019 Aaron Trickey. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    static let appName = "CueTimer"

    var window: NSWindow!
    var vc: CueTimerVC!

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        vc = CueTimerVC()
        window = NSWindow(contentViewController: vc)
        window.title = AppDelegate.appName

        window.makeKeyAndOrderFront(self)
    }

    func applicationShouldTerminateAfterLastWindowClosed(_ sender: NSApplication) -> Bool {
        return true
    }

    func application(_ sender: NSApplication, openFile filename: String) -> Bool {
        return vc.loadScript(at: URL(fileURLWithPath: filename))
    }

    override class func forwardingTarget(for selector: Selector!) -> Any? {
        switch selector {
        case #selector(getter: NSDocumentController.recentDocumentURLs),
             #selector(NSDocumentController.clearRecentDocuments(_:)),
             #selector(getter: NSDocumentController.maximumRecentDocumentCount):
            return NSDocumentController.shared

        default:
            return super.forwardingTarget(for: selector)
        }
    }
}

