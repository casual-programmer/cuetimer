
# CueTimer

I was recording a screen capture on my Mac, and had worked out a script: when
to click on this button, when to type in that text, and so on. I've got this
partly automated with Keyboard Maestro, but not all the way.

CueTimer is an app I wrote to sit on the screen and give me a timer, and show
me what to do next, with a countdown. The main window I was recording was next
to it on the screen. (I was using the Mac's built-in screen capture tool,
which lets you select a region of the screen to record.) The timer needed to
be big and clear, because I would mainly be looking that the window I was
using.

![Screenshot](screenshot.png)

## Usage

With no script: Run it. Click to start the timer. Click again to stop the
timer. Click again to reset the timer. Repeat as needed.

With a script: Create a JSON file that looks like this:

    {
      "events": [
        { "seconds": 10, "action": "message to show at 10 seconds" },
        { "seconds": 15.5, "action": "message to show at 15.5 seconds" },
        ...
      ]
    }

You don't have to sort the events, the app will do that for you.

Keep the messages short, the window isn't very big, but the font is.

Save it with a `.json` or `.cuetimer` extension.

Run CueTimer. Hit File > Open. Pick your JSON file. Click to start the
timer. Click again to stop the timer. Click again to reset the timer. Repeat
as needed.

## Building and Running

Load the project into Xcode and press the Run button.

## More info

Built in Swift for macOS, using plain old AppKit, in these heady days of
SwiftUI and Catalyst. And as an AppKit app, it's pretty lame; it should really
use NSDocument and provide a graphical editor, but I didn't want to spend much
time away from my [main product][timestory] which actually *uses* AppKit.

Copyright (c) 2019 Aaron Trickey. Free to use, modify, or share under the GNU
General Public License version 2.

 [timestory]: https://timestory.app

